#+TITLE: Räksalsa

4 bufféportioner/förrätt

* Ingredienser
:PROPERTIES:
:HTML_CONTAINER_CLASS: ingredients
:END:
- 200 g skalade räkor
- 2 tärnade mango eller 300 g frysta, tinade mangotärningar
- ½ tärnad gurka
- 2 strimlade salladslökar
- 1 urkärnad, finhackad röd chili
- pressad saft av 1 lime
- 1 msk muscovadosocker (gärna light brown)
- 1 kruka hackad koriander

* Gör så här
- Hacka räkorna grovt
- Blanda med mango, gurka, salladslök och chili.
- Blanda limesaft och socker i en skål.
- Häll blandningen över räksalsan och vänd runt med koriander. 

Källa: [[https://www.coop.se/globalassets/puffar/2013/toppsnurror/startsidan/mersmak1_2014.pdf][Mer smak nr 1 2014]]
