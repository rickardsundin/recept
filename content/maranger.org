#+TITLE: Maränger

Receptet ger 60 små maränger

* Ingredienser
:PROPERTIES:
:HTML_CONTAINER_CLASS: ingredients
:END:
- 3 st äggvitor
- 2 dl socker
- 1 krm ättika 

* Gör så här
- Separera vitorna från gulorna i en ren torr bunke. Det är viktigt att ingen gula kommer med eller att det finns någon annan typ av fett i bunken, för då blir det ingen maräng.
- Vispa kraftigt med elvisp tills ett fast skum uppstår, ca 2 min.
- Tillsätt ättikan och vispa ca 1 minut till.
- Tillsätt strösockret, lite i taget, under kraftigt vispande tills allt socker är blandat med äggvitorna. Fortsätt vispa tills du har ett blankt fast skum och du kan vända bunken upp-och-ned utan att marängsmeten rinner ut.
- Överkurs: Färga smeten med karamellfärg för ex rosa maränger eller vänd försiktigt ner några (2-3) matskedar chokladsås för chokladmaränger
- Fyll en sprits med smeten och spritsa ut maränger i valfri storlek på pappersklädda plåtar (det går åt 2 plåtar). Jag gillar att göra små maränger och då blir det ca 60 st av den här smeten. Man kan också klicka ut smeten med tesked, men då blir de såklart inte lika fina. :)
- Grädda i 100° varmluft (125° vanlig ugnsvärme, men varmluft är att föredra eftersom man då kan grädda båda plåtarna samtidigt) i ca 50 min-1 timme beroende på storlek. Plocka ut en maräng och smaka på innan du tar ut resten, det är så trist om de är blöta i mitten.

Källa: https://www.baka.se/recept/maranger
