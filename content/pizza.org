#+TITLE: Glutenfri pizza

Till 1 pizza.

* Ingredienser
:PROPERTIES:
:HTML_CONTAINER_CLASS: ingredients
:END:
- 30 g jäst
- 2 dl fingervarmt vatten
- 2 msk matolja
- 1 tsk salt
- 5,5 dl Glutenfri mjölmix (330 g)

* Gör så här

- Häll det fingervarma vattnet över jästen som smulats ner i en degbunke. Rör tills jästen är upplöst.
- Tillsätt olja, salt och mjölmix. Arbeta degen väl i maskin i ca 5 minuter.
- Låt jäsa i ca 30 minuter till dubbel storlek.
- Platta ut degen på bakpappersklädd plåt. Forma till en rund pizza, ca 20 cm i diameter och 1 cm tjock.
- Låt den jäsa 10-15 minuter. Sätt ugnen på 225º.
- Förgrädda pizzan i mitten av ugnen i 5-7 minuter. Täck därefter med garnering.
- Grädda i ytterligare 10-15 minuter beroende av garnering.

Garnera t ex med riven ost, finskuren rökt skinka eller korv, tomat, annanas och lök. Du kan frysa in förgräddade pizzabottnar. Du kan också byta ut den Glutenfria Mjölmixen mot Naturligt Glutenfri Mjölmix alt. Lågprotein Mjölmix.

Källa: [[http://finaxglutenfritt.se/recept/pizza]]
