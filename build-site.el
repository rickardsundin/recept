(require 'ox-publish)

(setq org-html-validation-link nil
      org-html-head-include-scripts nil
      org-html-head-include-default-style nil
      org-html-doctype "html5"
      org-html-html5-fancy t
      org-html-head "<link rel=\"stylesheet\" href=\"css/stylesheet.css\" />")

(setq org-publish-project-alist
      '(
        ("recipes"
         :recursive t
         :base-directory "./content"
         :publishing-directory "./public"
         :publishing-function org-html-publish-to-html
         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-title "Recept"
         :with-author nil
         :with-creator t
         :with-toc nil
         :section-numbers nil
         :time-stamp-file nil
         )
        ("static-assets"
         :recursive t
         :base-directory "./assets"
         :publishing-directory "./public"
         :base-extension any
         :publishing-function org-publish-attachment
         )
        ("all"
         :components ("recipes" "static-assets")
         )))

(org-publish-all t)

(message "Build complete")
